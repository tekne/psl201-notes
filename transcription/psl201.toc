\contentsline {section}{\numberline {1}Cell Transport and Signalling}{3}{section.1}% 
\contentsline {subsection}{\numberline {1.1}Parts of a cell}{3}{subsection.1.1}% 
\contentsline {subsection}{\numberline {1.2}Transport}{3}{subsection.1.2}% 
\contentsline {section}{\numberline {2}The Endocrine System}{3}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Cell-to-Cell Communication}{3}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}The Hypothalamus and Pituitary Gland}{4}{subsection.2.2}% 
\contentsline {subsubsection}{\numberline {2.2.1}ADH: Vasopressin}{5}{subsubsection.2.2.1}% 
\contentsline {subsubsection}{\numberline {2.2.2}Oxytocin}{5}{subsubsection.2.2.2}% 
\contentsline {subsubsection}{\numberline {2.2.3}Anterior Pituitary}{5}{subsubsection.2.2.3}% 
\contentsline {subsubsection}{\numberline {2.2.4}Human Growth}{5}{subsubsection.2.2.4}% 
\contentsline {subsection}{\numberline {2.3}Calcium Balance}{6}{subsection.2.3}% 
\contentsline {subsubsection}{\numberline {2.3.1}Bone}{6}{subsubsection.2.3.1}% 
\contentsline {subsubsection}{\numberline {2.3.2}Hormonal Regulation of Plasma Calcium}{6}{subsubsection.2.3.2}% 
\contentsline {subsection}{\numberline {2.4}Thyroid}{6}{subsection.2.4}% 
\contentsline {subsubsection}{\numberline {2.4.1}Synthesis of Thyroid Hormones}{7}{subsubsection.2.4.1}% 
\contentsline {subsubsection}{\numberline {2.4.2}Action of Thyroid Hormones}{7}{subsubsection.2.4.2}% 
\contentsline {subsubsection}{\numberline {2.4.3}Thyroid Hormone Disorders}{7}{subsubsection.2.4.3}% 
\contentsline {subsection}{\numberline {2.5}Adrenal}{8}{subsection.2.5}% 
\contentsline {subsubsection}{\numberline {2.5.1}Aldosterone}{8}{subsubsection.2.5.1}% 
\contentsline {subsubsection}{\numberline {2.5.2}Glucocorticoids (Cortisol)}{9}{subsubsection.2.5.2}% 
\contentsline {subsubsection}{\numberline {2.5.3}Adrenal Disorders}{9}{subsubsection.2.5.3}% 
\contentsline {subsubsection}{\numberline {2.5.4}Epinephrine}{9}{subsubsection.2.5.4}% 
\contentsline {subsection}{\numberline {2.6}Energy Metabolism}{10}{subsection.2.6}% 
\contentsline {subsubsection}{\numberline {2.6.1}Absorptive and Postabsorptive States}{10}{subsubsection.2.6.1}% 
\contentsline {subsubsection}{\numberline {2.6.2}Hormonal Regulation of Metabolism}{11}{subsubsection.2.6.2}% 
\contentsline {subsection}{\numberline {2.7}The Reproductive System}{11}{subsection.2.7}% 
\contentsline {subsubsection}{\numberline {2.7.1}The Male Reproductive System}{12}{subsubsection.2.7.1}% 
\contentsline {subsubsection}{\numberline {2.7.2}The Female Reproductive System}{13}{subsubsection.2.7.2}% 
\contentsline {section}{\numberline {3}Nerve Cells and Electrical Signaling}{14}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Overview of the Nervous System}{14}{subsection.3.1}% 
\contentsline {subsection}{\numberline {3.2}Cells of the Nervous System}{14}{subsection.3.2}% 
\contentsline {subsubsection}{\numberline {3.2.1}Neurons}{14}{subsubsection.3.2.1}% 
\contentsline {subsubsection}{\numberline {3.2.2}Glial Cells and Myelination}{14}{subsubsection.3.2.2}% 
\contentsline {subsubsection}{\numberline {3.2.3}Ion Channels}{14}{subsubsection.3.2.3}% 
\contentsline {subsection}{\numberline {3.3}Muscle Contraction}{15}{subsection.3.3}% 
\contentsline {section}{\numberline {4}Cardiovascular Physiology}{15}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Cardiac Action Potentials}{16}{subsection.4.1}% 
\contentsline {section}{\numberline {5}Blood and Immunity}{16}{section.5}% 
\contentsline {subsection}{\numberline {5.1}Erythrocytes}{17}{subsection.5.1}% 
\contentsline {subsubsection}{\numberline {5.1.1}Anemia, Jaundice and Polycythemia}{17}{subsubsection.5.1.1}% 
\contentsline {subsubsection}{\numberline {5.1.2}Blood Types}{18}{subsubsection.5.1.2}% 
\contentsline {subsection}{\numberline {5.2}Components of the Immune System}{18}{subsection.5.2}% 
\contentsline {subsection}{\numberline {5.3}Innate Immunity}{18}{subsection.5.3}% 
\contentsline {subsection}{\numberline {5.4}Specific Immunity}{19}{subsection.5.4}% 
\contentsline {subsubsection}{\numberline {5.4.1}Humoral Immunity}{19}{subsubsection.5.4.1}% 
\contentsline {subsubsection}{\numberline {5.4.2}Cell Mediated Immunity}{20}{subsubsection.5.4.2}% 
\contentsline {section}{\numberline {6}The Resipiratory System}{20}{section.6}% 
\contentsline {subsection}{\numberline {6.1}Structures of the Respiratory System}{21}{subsection.6.1}% 
\contentsline {subsubsection}{\numberline {6.1.1}Upper Airways and Respiratory Tract}{21}{subsubsection.6.1.1}% 
\contentsline {subsubsection}{\numberline {6.1.2}Structures of the Thoracic Cavity}{21}{subsubsection.6.1.2}% 
\contentsline {subsection}{\numberline {6.2}Pulmonary Circulation}{21}{subsection.6.2}% 
\contentsline {subsection}{\numberline {6.3}Breathing}{21}{subsection.6.3}% 
\contentsline {subsection}{\numberline {6.4}Gas Exchange and Transport}{22}{subsection.6.4}% 
\contentsline {subsection}{\numberline {6.5}Control of Respiration}{22}{subsection.6.5}% 
\contentsline {section}{\numberline {7}Renal Physiology}{23}{section.7}% 
\contentsline {subsection}{\numberline {7.1}Glomerular Filtration}{24}{subsection.7.1}% 
\contentsline {subsection}{\numberline {7.2}Glomerular Reabsorbtion and Secretion}{24}{subsection.7.2}% 
\contentsline {subsection}{\numberline {7.3}Water and Sodium Balance}{24}{subsection.7.3}% 
\contentsline {subsection}{\numberline {7.4}Electrolyte and Acid-Base Balance}{24}{subsection.7.4}% 
\contentsline {subsubsection}{\numberline {7.4.1}Renin-angiotensin-aldosterone system (RAAS)}{24}{subsubsection.7.4.1}% 
\contentsline {section}{\numberline {8}The Gastrointestinal System}{24}{section.8}% 
\contentsline {subsection}{\numberline {8.1}Functional Anatomy}{24}{subsection.8.1}% 
\contentsline {subsubsection}{\numberline {8.1.1}From Mouth to Stomach}{24}{subsubsection.8.1.1}% 
\contentsline {subsubsection}{\numberline {8.1.2}The Small Intestine}{25}{subsubsection.8.1.2}% 
\contentsline {subsubsection}{\numberline {8.1.3}The Large Intestine}{25}{subsubsection.8.1.3}% 
\contentsline {subsubsection}{\numberline {8.1.4}The Liver, Gallbladder, and Pancreas}{25}{subsubsection.8.1.4}% 
\contentsline {subsection}{\numberline {8.2}Gastric Motility}{26}{subsection.8.2}% 
\contentsline {subsubsection}{\numberline {8.2.1}Electrical Activity in GI Smooth Muscle}{26}{subsubsection.8.2.1}% 
\contentsline {subsubsection}{\numberline {8.2.2}Mouth and Esophagus}{27}{subsubsection.8.2.2}% 
\contentsline {subsubsection}{\numberline {8.2.3}Stomach}{27}{subsubsection.8.2.3}% 
\contentsline {subsubsection}{\numberline {8.2.4}Small Intestine}{27}{subsubsection.8.2.4}% 
\contentsline {subsubsection}{\numberline {8.2.5}Colon}{27}{subsubsection.8.2.5}% 
\contentsline {subsubsection}{\numberline {8.2.6}Regulation of Gastric Motility}{27}{subsubsection.8.2.6}% 
\contentsline {subsection}{\numberline {8.3}Digestion and Absorbtion}{27}{subsection.8.3}% 
\contentsline {subsubsection}{\numberline {8.3.1}Carbohydrates}{28}{subsubsection.8.3.1}% 
\contentsline {subsubsection}{\numberline {8.3.2}Proteins}{29}{subsubsection.8.3.2}% 
\contentsline {subsubsection}{\numberline {8.3.3}Lipids}{29}{subsubsection.8.3.3}% 
\contentsline {subsubsection}{\numberline {8.3.4}Vitamins}{30}{subsubsection.8.3.4}% 
\contentsline {subsubsection}{\numberline {8.3.5}Water}{30}{subsubsection.8.3.5}% 
\contentsline {subsection}{\numberline {8.4}Secretions and General Regulation}{30}{subsection.8.4}% 
